# Homepage of kermit

### LICENSE

The files in this archive are released under BSD-2-Clause license.
You can find a copy of this license in [LICENSE](https://gitlab.com/kermit-js/kermit-js.gitlab.io/raw/master/LICENSE).