---
title: kermitJS
date: 2016-07-05T12:49:02.197Z
type: index
weight: 0
---

[![NPM](https://nodei.co/npm/kermit.png?downloads=true)](https://nodei.co/npm/kermit/)
---
[![build status](https://ci.gitlab.com/projects/3656/status.png?ref=master)](https://ci.gitlab.com/projects/3656?ref=master)
[![coverage status](https://coveralls.io/repos/kermit-js/kermit/badge.svg?branch=master&service=github)](https://coveralls.io/github/kermit-js/kermit?branch=master)

# kermit - 2.2.0

- is the **infrastructure** for service oriented architecture (**SOA**) for node.js
- provides **unified interfaces** for writing **modular** apps and (micro-)**services**
- eases and unifies the **configuration** of apps and its modules
- enables **dependency injection**
- is written in **ES6** with **ES5** compatible build using babel
- is fully **tested** with mocha

---
Find the api docs on [kermit-js.readme.io](https://kermit-js.readme.io)

---

## The Doctrine

1. An application is complex. So lets split it into functional modules (**Services**),
each of them being way more **simple** and **exchangeable**.
2. All Services have an unified interface `configure([serviceConfig])`, `bootstrap()`, `launch()`.
3. All Services are **EventEmitters**.
4. The **ServiceManager** is the di-container of kermit in which all services are registered.
5. All Services have access to the ServiceManager.
6. An Application is a Service, that manages dependent services and their configuration in the ServiceManager.
7. An Application manages the life-cycle of its dependent services.


### The Vision

- There are many kermit wrappers for popular node.js modules.
- kermit is ported to other environments / programming languages.


## Install

`$ npm install kermit`


## Try Kermit

[![Try Kermit on runnable.com](http://i.imgur.com/F2AAzgs.png)](https://code.runnable.com/kermit)

### TODO

- Improve the docs
    - Examples
- Write third party module wrappers


### CHANGELOG

Please have a look at [CHANGELOG](https://gitlab.com/kermit-js/kermit/raw/master/CHANGELOG).


### LICENSE

The files in this archive are released under BSD-2-Clause license.
You can find a copy of this license in [LICENSE](https://gitlab.com/kermit-js/kermit/raw/master/LICENSE).